package pydub;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.apache.commons.io.IOUtils;
import org.bytedeco.javacpp.Loader;
import org.testng.collections.Lists;

import com.google.common.base.Joiner;

import cn.hutool.core.io.FileUtil;
import net.coobird.thumbnailator.Thumbnails;
import util.WorkId;

/**
 * 提取字幕
 * @author Administrator
 * FFmpeg 从视频中提取图片
	ffmpeg -i test.mp4 -r 30 image-%3d.jpg
	ffmpeg -i test.mp4 -r 30 -t 4 image-%3d.jpg
	ffmpeg -i test.mp4 -r 30 -ss 00:00:20 image-%3d.jpg
	ffmpeg -i test.mp4 -r 30 -ss 00:00:20 -vframes 10 image-%3d.jpg
	ffmpeg -i test.mp4 -r 30 image-%3d.jpg
	text.mp4视频文件
	-r 30 每秒提取30帧，一般是24帧
	image-%3d 文件命名格式是image-001.jpg
	
	ffmpeg -i test.mp4 -r 30 -t 4 image-%3d.jpg
	-t，表示取t秒时间的帧
	
	ffmpeg -i test.mp4 -r 30 -ss 00:00:20 image-%3d.jpg
	-ss，表示截取帧初始时间
	
	ffmpeg -i test.mp4 -r 30 -ss 00:00:20 -vframes 10 image-%3d.jpg
	-vframes，表示截取多少帧
	
	
	利用ffmpeg就可以完成这项任务了，下面是具体指令，再附上每个指令的说明：
	-threads：线程，这里可以设2个，加快视频生成速度
	-y：图片合成视频会有很多个覆盖操作，此处一律同意覆盖
	-r：fps，这个是关键，设置多少帧/s，这里我自己设的是8，就是每八张图片合成1s的视频
	-i：后接的第一个参数是输入的图片，我的图片都是以image%06d命名的，按自己图片的实际命名排序来设置，最后的是输出视频的格式。
	ffmpeg -threads 2 -y -r 8 -i image%06d.jpg 1234.mp4
	ffmpeg  -y -r 25 -i frames/out-%03d.jpg 1234.mp4
 
	图片合成视频：
	ffmpeg -f image2 -i frames/out-%03d.jpg -r 25 video.mp4
	
	ffmpeg -y -i 原视频地址 -r 原视频帧率 -i "帧图地址/%04d.png" -map 0:a? -map 1:v -r 生成视频帧率 -c:v libx264 -b:v 8M -pix_fmt yuv420p -c:a aac -strict -2 -b:a 192k -ar 48000 -strict -2 "生成视频地址/result.mp4" -loglevel error
 	
 	提取音频：
 	ffmpeg -i c:/db/1.mp4 -f mp3 -vn c:/db/1.mp3
 	提取视频：
 	ffmpeg -i c:/db/1.mp4 -vcodec copy –an c:/db/an1.mp4
 	
 	
 	ffmpeg图片生成mp4添加歌曲
	首先图片合成视频
	ffmpeg -f image2 -r 1.6 -i ./photo/%d.jpeg ./rst3.mp4
	-r 1.6秒 间隔可以自由设置
	裁切视频
	ffmpeg -ss 00:00:00 -t 00:03:03 -i out.mp4 -vcodec copy -acodec copy kk.mp4
	从0开始 裁切前3分03秒
	
	合并音视频,添加歌曲
	ffmpeg -i rst.mp4 -i qbgn.m4a  out.mp4
 	
 	

	参数介绍
	-loop 1 ：因为只有一张图片所以必须加入这个参数（循环这张图片）
	-pix_fmt：指定图片输入格式(有yuv420,yuv444等各种格式)
	-t ：图片转换成视频的持续时长，单位是秒（S），必须指定该值，否则会无限制生成视频
	-s ：指定视频的分辨率
	-vcodec libx264：生成视频的编码格式，这里指定的是x264
 	ffmpeg -r 25 -f image2  -i frames/out-%03d.jpg -i video/1.mp3 -pix_fmt yuvj420p  -vcodec libx264 video/456.mp4
 	
 	How to super-resolution video
	Converts videos into serialized images.
	
	ffmpeg -i test.mp4 -vcodec png main/LR/calendar/image_%04d.png
	super-resolution
	tecoGAN.bat
	
	output -> main/results/output_%03d.png To return the output result to a movie, do as follows.
	
	ffmpeg -r 24 -i output_%04d.png -vcodec libx264 -pix_fmt yuv420p -r 24 test.mp4
	
	Change the codec and format accordingly. Since "24" in the command is the frame rate, enter the frame rate of the original movie.
 	
 	
 */
public class FfmpegVideoStream {
	
	static String likeStr = "subtitle";
	
	public static void main(String[] args) throws InterruptedException, IOException {
		 String ffmpeg = Loader.load(org.bytedeco.ffmpeg.ffmpeg.class);
		//获取视频帧信息
		 //ProcessBuilder voideInfo = new ProcessBuilder(ffmpeg, "-i", "F:/face/vido/1.avi");
		 //voideInfo.inheritIO().start().waitFor();
		 //按频率获取全部图片
		 /*ProcessBuilder pb = new ProcessBuilder(ffmpeg, "-i", "F:/face/vido/1.avi","-r","25","image-%3d.jpg"); 
		 InputStream in = pb.start().getInputStream();*/
		 //获取某个图片
		 /*ProcessBuilder pb = new ProcessBuilder(ffmpeg, "-i", "F:/face/vido/1.avi","-ss","10","-f","image2","image.jpg"); 
		 InputStream in = pb.start().getInputStream();*/
		 //取出图片
		 /*ProcessBuilder pb = new ProcessBuilder(ffmpeg, "-i", "F:/face/vido/1.avi","frames/out-%03d.jpg"); 
		 InputStream in = pb.start().getInputStream();*/
		 //提取音频
		/* ProcessBuilder pb = new ProcessBuilder(ffmpeg, "-i", "F:/face/vido/1.avi","-f","mp3","-vn","video/1.mp3"); 
		 InputStream in = pb.start().getInputStream();*/ 
		 //图片合成mp4
		 /*ProcessBuilder pb = new ProcessBuilder(ffmpeg,"-y","-r","25", "-i", "frames/out-%03d.jpg","video/123.mp4"); 
		 InputStream in = pb.start().getInputStream();*/ 
		 //合并图片和mp3
		 /*ProcessBuilder pb = new ProcessBuilder(ffmpeg,"-y","-r","25","-f","image2","-i","frames/out-%03d.jpg","-i","video/1.mp3","-pix_fmt","yuvj420p","-c:v","libx264","-c:a","copy","-map","0:0","-map","1:a","video/456.mp4"); 
		 pb.inheritIO().start().waitFor();*/
		 //Thumbnails.of(in).scale(1).outputFormat("jpg").toFile("F:/face/vido/"+WorkId.sortUID()+".jpg");
		/* ProcessBuilder pb = new ProcessBuilder(ffmpeg, "-i", "D:/AI/浪迹天涯.H263.480P.01.mkv","D:/AI/frames/out-%05d.jpg"); 
		 InputStream in = pb.start().getInputStream();*/
		 
		 
		 
		 FileInputStream fis=new FileInputStream("E:\\code\\git_work\\ttskit-main\\ttskit\\resource\\audio\\biaobei-biaobei-009502.mp3");     
         
         try {     
             InputStreamReader isr=new InputStreamReader(fis,"utf8");                     
             BufferedReader br=new BufferedReader(isr);   
             String line; 
             List<String> out_parts = Lists.newArrayList();
             while((line=br.readLine()) != null){     
                 System.out.println(line); 
                 out_parts.add(line);
                 Joiner.on("").join(out_parts);
                 
             }     
         } catch (Exception e) {     
             // TODO Auto-generated catch block     
             e.printStackTrace();     
         }         
		 
		 
		 
		 
		 
		 ProcessBuilder pb = new ProcessBuilder(ffmpeg, "-i",
				 "E:\\code\\git_work\\ttskit-main\\ttskit\\resource\\audio\\biaobei-biaobei-009502.mp3",
				 "-f","s16le","-"); 
		 InputStream input = pb.start().getInputStream();
		 byte[] data = new byte[1024]; 
		 int rc = 0;
		 Queue<byte[]> queue = new LinkedList<byte[]>();
         while ((rc = input.read(data)) > 0) {
        	 queue.add(data);
         }	
	     input.close();
	     System.out.println(queue);
	     queue.forEach(qu ->{
	    	 
	     });
		 System.out.println("===========");
	}
	
}

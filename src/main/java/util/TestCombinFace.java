package util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bytedeco.opencv.global.opencv_core;
import org.bytedeco.opencv.global.opencv_imgcodecs;
import org.bytedeco.opencv.global.opencv_imgproc;
import org.bytedeco.opencv.opencv_core.Mat;
import org.bytedeco.opencv.opencv_core.Size;

import ai.djl.inference.Predictor;
import ai.djl.modality.cv.Image;
import ai.djl.modality.cv.ImageFactory;
import ai.djl.modality.cv.util.NDImageUtils;
import ai.djl.ndarray.NDArray;
import ai.djl.ndarray.NDManager;
import ai.djl.ndarray.types.DataType;
import ai.djl.ndarray.types.Shape;
import ai.djl.repository.zoo.Criteria;
import ai.djl.repository.zoo.ModelZoo;
import cn.hutool.core.io.FileUtil;
import net.coobird.thumbnailator.Thumbnails;
import vfile.translate.NPtKTranslator;
import vfile.translate.PtGTranslator;

public class TestCombinFace { 
	
	public static void main(String[] args) throws Exception {
		//说了你可能不行，这个就是为了解决bug的
    	Mat one = new Mat();
    	System.setProperty("ai.djl.default_engine", "PyTorch");
    	String videoType = "mp4";  
    	
    	
    	File[] files = FileUtil.ls("E:/code/git_work/FaceMerge/build/output/2");
    	List<Image> driving_video = new ArrayList(); 
    	for(File file : files){
    		driving_video.add(ImageFactory.getInstance().fromFile(Paths.get(file.getAbsolutePath()))); 
    	} 
		Path changeImg = Paths.get("src/main/resources/img/2m.jpg");
		
		Image image = ImageFactory.getInstance().fromFile(changeImg);
		List<BufferedImage> buf = test(image,driving_video);
		int total = driving_video.size();
		for(int i=0;i<total;i++){
			
			//Thumbnails.of(b).scale(1).outputFormat("jpg").toFile("E:/code/git_work/FaceMerge/build/output/"+"3/"+WorkId.sortUID());
			Mat newImg1 = ConverterImg.b2M2(buf.get(i),opencv_core.CV_8UC3);
    		opencv_imgproc.resize(newImg1, newImg1, new Size(256,256)); 
    		
    		Mat oldImg = ConverterImg.b2M2((BufferedImage)driving_video.get(i).getWrappedImage(),opencv_core.CV_8UC3); 
    		
    		
    		//List<Mat> oldFace = FeatureDetection.faceDetection(oldImg);
    		//List<Mat> newFace = FeatureDetection.faceDetection(newImg); 
    		
    		//此处添加人脸比对 ， 先默认获取第一张人脸
    		
    		Mat combinFace = OpenCVFaceVideoSwap.faceMerge(newImg1,oldImg,false); 
    		//Thumbnails.of(bImg).scale(1).outputFormat("jpg").toFile("E:/code/git_work/FaceMerge/build/output/"+"2/"+WorkId.sortUID());
    		opencv_imgcodecs.imwrite("E:/code/git_work/FaceMerge/build/output/"+"3/"+WorkId.sortUID()+".jpg", combinFace);
    		
		} 
		/*Path vidoePath = Paths.get("src/main/resources/video/ds."+videoType);
        Path pushPath = Paths.get("build/output/file2."+videoType);
		VideoTool.push(vidoePath.toString(), pushPath.toString(), buf,videoType); */
	}
	public static List<BufferedImage> test(Image image,List<Image> driving_video) throws Exception {
    	
    	
    	String videoType = "mp4";  

        NDManager manager = NDManager.newBaseManager();
        NDArray img = change(image,manager); 
        List<BufferedImage> resultVideo = new ArrayList();

        Criteria<NDArray, Map> kpDetector =
                Criteria.builder()
                        .setTypes(NDArray.class, Map.class)
                        .optTranslator(new NPtKTranslator())
                        .optEngine("PyTorch")
                        .optModelPath(Paths.get("F:/1/model/kpdetector.pt"))
                        .build();
        Predictor<NDArray, Map> kPredictor = ModelZoo.loadModel(kpDetector).newPredictor();

       
        //saveBoundingBoxImage(driving_video.get(0));
        Criteria<List, Image> generatorCr =
                Criteria.builder()
                        .setTypes(List.class, Image.class)
                        .optEngine("PyTorch")
                        .optTranslator(new PtGTranslator())
                        .optModelPath(Paths.get("F:/1/model/generator.pt"))
                        .build();

        Predictor<List, Image> generator = ModelZoo.loadModel(generatorCr).newPredictor();
        
        Map kp_source = kPredictor.predict(img); 
        
        Map kp_driving_initial = kPredictor.predict(change(driving_video.get(0),manager));
        int count = 0;
        int total = driving_video.size();
        //进度条打印
        ConsoleProgressBar bar = new ConsoleProgressBar(total);
		 
        for(Image dimg : driving_video){
        	
        	bar.draw(count++, 1);  
            List<Object> g = new ArrayList<>();
            Map kp_driving = kPredictor.predict(change(dimg,manager));
            //Map kp_norm = ImgTool.normalize_kp(kp_source, kp_driving, kp_driving_initial, 1, true, true);
            //System.out.println(kp_norm);
            g.add(img);
            g.add(kp_driving);
            g.add(kp_source);
            g.add(kp_driving_initial);
            resultVideo.add((BufferedImage)generator.predict(g).getWrappedImage()); 
        }  
        manager.close();
        
        return resultVideo;
    }
    private static NDArray change(Image img, NDManager manager){
    	NDArray driving0 = img.toNDArray(manager); 
        driving0 = NDImageUtils.resize(driving0, 256, 256);
        driving0 = driving0.div(255); 
        driving0 = driving0.transpose(2, 0, 1); 
        driving0 = driving0.toType(DataType.FLOAT32,false);
        driving0 = driving0.broadcast(new Shape(1,3,256,256));
        return driving0;
    }
    private static void saveBoundingBoxImage(Image img)
            throws IOException {
        Path outputDir = Paths.get("build/output");
        Files.createDirectories(outputDir);

        // Make image copy with alpha channel because original image was jpg
        Image newImage = img.duplicate(Image.Type.TYPE_INT_ARGB); 
        Path imagePath = outputDir.resolve(WorkId.sortUID()+".png");
        // OpenJDK can't save jpg with alpha channel
        newImage.save(Files.newOutputStream(imagePath), "png");
       
    } 
    
}
